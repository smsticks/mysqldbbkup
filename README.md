# README #

MySQL backup and import scripts using bash/shell from terminal.

### What is this repository for? ###

* Version: 1.0
* Quick summary

I created these scripts due to working with lots of databases, tables and large amounts of data.  It wasn't convenient to backup and restore this data through a UI like Sequel Pro, especially when you're handling gigs of data.  So, these scripts helped a ton.  

NOTE: Once you run the backup_sql.sh your files will be Gzipped (.gz) for compression.  If you need to run the import_sql.sh script, be sure to Gunzip these files before importing.

```
gunzip -r ./data/{database}*.gz
```


I'll have to adjust the script and add a command to gunzip before importing.


### How do I get set up? ###

* Database configuration

You'll need access to your local database in the terminal, please Google how to do this.  I have the scripts using root/root for username and password.  I recall you have to run a command to allow MySQL to set the root password (again, please Google this).  Otherwise, try using a user account that has access to your databases.

### Who do I talk to? ###

* questions: [tdsticks@gmail.com](tdsticks@gmail.com)