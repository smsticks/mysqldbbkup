#!/bin/bash

# Note: If you're using this in correspondence to the bkup_sql.sh,
# then be sure to gunzip the files before importing!

# Path to where you pull the data from
datapath=./data

# List of databases
databases=(
"First_DB_Name"
"Second_DB_Name"
"Third_DB_Name"
)

# localhost
host='127.0.0.1'

# DB Credentials
user=root
pwd=root

# Loop through the databases
for db in "${databases[@]}"
do
    echo DATABASE: $db

    path=$datapath/$db
    # echo "  path" $path

    # Get the table files (.sql)
    tables_ary=(`ls -1 $path/*.sql`)

    # Loop through the table names
    for t in "${tables_ary[@]}"
    do
        # echo $t

        # file_name=${t%.sql}
        # table_name=${file_name##*/}
        # echo $table_name

        # Import the table into the database
        echo "mysql -h $host -u $user -p$pwd $db < $t"
        mysql -h $host -u $user -p$pwd $db < $t
    done
done