#!/bin/bash

# List of databases
databases=(
"First_DB_Name"
"Second_DB_Name"
"Third_DB_Name"
)

# DB Credentials
user=root
pwd=root

# Path to where you want the db bkups to go
datapath=data

# Loop through each database, make a dir using the db name,
# then export each table within that db and also Gzip them
for db in "${databases[@]}"
do
	echo DATABASE: $db

	path=$datapath/$db
	# echo "  path" $path

	# Make the db folder (only need to run this once!)
	mkdir $path

	# Select the table names from the db
	tables=$(mysql $db -u $user -p$pwd -e 'show tables' | grep -v Tables_in)

	# Loop through the table names and dump out the table and Gzip it
	for t in $tables
	do
		# echo "	" $t
		echo "	mysqldump -p --user=$user -p$pwd $db $t | gzip -9 > $path/$t.sql.gz"
		mysqldump -p --user=$user -p$pwd $db $t | gzip -9 > $path/$t.sql.gz
	done
done